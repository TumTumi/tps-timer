# README #

Dieser Timer soll helfen seine Zeit einzuhalten. Es ist ein Prototype für einen Hardware Timer.
Kann aber auch genutzt werden für sein Mobile Devices.

### Zusammenfassung ###

* 90% JavaScript, einmal laden im Browser und ist immer nutzbar
* Vorgegeben Zeit Einheiten
* Warnt wenn die Zeit vorbei ist
* Zählt ab oder auf

### Installation ###

* Git downloaden, auf einen einfachen Webserver packen und auf diese Seite Surfen

### Autoren ###
Steve S.
Tobias M.

