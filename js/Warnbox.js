/**
 * Created by tobi on 10.04.15.
 */

function Warnbox() {
    var me = this;

    me.configAutostopMin = 1;
    me.warnboxElement = null;
    me.warnboxTimeID = null;
    me.autostopTimeID = null;
    me.warnboxBlinkMode = false;

    me.init();
}

/**
 * Inistaliesier
 */
Warnbox.prototype.init = function () {
    var me = this,
        div = document.getElementById('warnmeldung'),
        parent = div.parentNode;
    parent.removeChild(div);
    div.style.display = "block";
    me.warnboxElement = div;
};

/**
 * Zeigt die Box an im Element
 * @param inElement
 */
Warnbox.prototype.show = function (inElement) {
    var me = this;
    me.stopBlink();
    inElement.innerHTML = "";
    inElement.appendChild(me.warnboxElement);
    me.startBlink();
};

/**
 * Entfernt die Warnbox
 */
Warnbox.prototype.hide =  function () {
    var me = this,
        parent = me.warnboxElement.parentNode;
    if (parent) {
        parent.removeChild(me.warnboxElement);
    }
    me.stopBlink();
    me.clearBlock();
};

/**
 * Start Blink
 */
Warnbox.prototype.startBlink = function () {
    var me = this;
    me.warnboxTimeID = window.setInterval(
        function () { me.event_Blink(); },
        400
    );
    me.autostopTimeID = window.setTimeout(
        function () { me.event_autostop(); },
        1000 * 60 * me.configAutostopMin
    );
};

/**
 * Stopt Blink
 */
Warnbox.prototype.stopBlink = function () {
    var me = this;
    if (me.warnboxTimeID !== null) {
        window.clearInterval(me.warnboxTimeID);
        me.warnboxTimeID = null;
    }
    if (me.autostopTimeID !== null) {
        window.clearTimeout(me.autostopTimeID);
        me.autostopTimeID = null;
    }
};

/**
 * TimerInterval wo die Box Blinkt
 */
Warnbox.prototype.event_Blink = function () {
    var me = this;
    me.styleBlinkBox(me.warnboxBlinkMode);
    me.warnboxBlinkMode = !me.warnboxBlinkMode;
};

/**
 * TimerInterval auto Stop eine Uhr die Automaitsch ended
 */
Warnbox.prototype.event_autostop = function () {
    var me = this;
    me.hide();
};

/**
 * verändert das aussehen der Box
 * @param mode
 */
Warnbox.prototype.styleBlinkBox = function (mode) {
    var me = this,
        background_color =  mode ? "black" : "white",
        text_color = mode ? "white" : "black";

    me.warnboxElement.style.color = text_color;
    me.warnboxElement.style.backgroundColor = background_color;
};

/**
 * Weiße Meldung
 * @param mode
 */
Warnbox.prototype.clearBlock = function () {
    var me = this;
    me.styleBlinkBox(true);
};