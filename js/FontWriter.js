/**
 * Created by tobi on 10.04.15.
 */

function FontWriter() {

    this.font = {
        "banner3": {
            "-": ["       ", "       ", "       ", "#######", "       ", "       ", "       "],
            ":": ["  ##  ", " #### ", "  ##  ", "      ", "  ##  ", " #### ", "  ##  "],
            "1": ["   ##  ", " ####  ", "   ##  ", "   ##  ", "   ##  ", "   ##  ", " ######"],
            "2": ["  ####### ", " ##     ##", "        ##", "  ####### ", " ##       ", " ##       ", " #########"],
            "3": ["  ####### ", " ##     ##", "        ##", "  ####### ", "        ##", " ##     ##", "  ####### "],
            "4": [" ##       ", " ##    ## ", " ##    ## ", " ##    ## ", " #########", "       ## ", "       ## "],
            "5": [" ########", " ##      ", " ##      ", " ####### ", "       ##", " ##    ##", "  ###### "],
            "6": ["  ####### ", " ##     ##", " ##       ", " ######## ", " ##     ##", " ##     ##", "  ####### "],
            "7": [" ########", " ##    ##", "     ##  ", "    ##   ", "   ##    ", "   ##    ", "   ##    "],
            "8": ["  ####### ", " ##     ##", " ##     ##", "  ####### ", " ##     ##", " ##     ##", "  ####### "],
            "9": ["  ####### ", " ##     ##", " ##     ##", "  ########", "        ##", " ##     ##", "  ####### "],
            "0": ["   #####  ", "  ##   ## ", " ##     ##", " ##     ##", " ##     ##", "  ##   ## ", "   #####  "]
        }
    };

    this.fontLibrary = new FontLibrary();
    this.setDisplayMode = "dotmatix";
    this.setFont(this.font.banner3);
    this.init();
}

/**
 * Gibt die Schrift in einer ASCI Font zurück
 * @param output
 * @returns {string}
 */
FontWriter.prototype.printNummer = function (output) {
    var me = this,
        stdout = "";

    me.output = output;
    if (me.isDotDisplayMode()) {
        stdout = "<pre>" + me.writeLines() + "</pre>";
    } else if (me.isLCDDisplayMode()) {
        stdout = "<div class='lcd'>" + output + "</div>";
    } else {
        stdout = output;
    }

    return stdout;
};

/**
 * Initialisierung
 */
FontWriter.prototype.init = function () {
    var me = this;
    me.event_selectFont();
};

/**
 * Erstellt den event für die Schrift umschreibung
 */
FontWriter.prototype.event_selectFont = function () {
    var me = this,
        fontSelector = document.getElementById('fontType');

    fontSelector.onchange = function (e) {
        var option = fontSelector[this.selectedIndex];
        if (option.value === "lcd") {
            me.setLCDDisplayMode();
            return;
        }
        me.assignFontFamily(option.value);
    };
};

/**
 * Setzt die Zeilen höhe fest damit die Buchstaben gerneriert werden.
 * Nebenbei prüft es ob alle Buchstaben gleich hoch sind.
 * @param key
 */
FontWriter.prototype.setLetterHight = function (key) {
    var me = this,
        arrayDigt = me.fontFamily[key];

    if (me.letterHigth !== arrayDigt.length && me.letterHigth !== 0) {
        console.log("Dieser Buchstabe `" + key + "` hat eine falsche Hoehe.");
    } else {
        me.letterHigth = arrayDigt.length;
    }
};

/**
 * Befült jede zeile aus. So hoch wie die Buchstaben sind
 * @returns {string}
 */
FontWriter.prototype.writeLines = function () {
    var me = this,
        stdout = "",
        line;
    for (line = 0; line < me.letterHigth; line++) {
        stdout += me.writeColumn(line);
        stdout += "\n";
    }
    return stdout;
};

/**
 * Befült die Zeile mit den Buchstaben Informationen
 * @param line
 * @returns {string}
 */
FontWriter.prototype.writeColumn = function (line) {
    var me = this,
        stdline = "",
        output_length = me.output.length,
        digt_indx,
        digt;

    for (digt_indx = 0; digt_indx < output_length; digt_indx++) {
        digt = me.output[digt_indx];
        if (me.fontFamily[digt]) {
            stdline += me.fontFamily[digt][line];
        }
    }

    return stdline;
};

/**
 * Setzt die aktive Font die gebraucht wird
 * @param fontJson
 */
FontWriter.prototype.setFont = function (fontJson) {
    var me = this,
        key;
    me.setDotDisplayMode();
    me.fontFamily = fontJson;
    me.letterHigth = 0;
    for (key in me.fontFamily) {
        me.setLetterHight(key);
    }
};

/**
 * Setzt Display auf Dot Matrix
 */
FontWriter.prototype.setDotDisplayMode = function () {
    this.setDisplayMode = "dotmatix";
};

/**
 * Setzt das Distplat auf LCD Modus
 */
FontWriter.prototype.setLCDDisplayMode = function () {
    this.setDisplayMode = "LCD";
};

/**
 * Soll im Dot Matrix Modus erscheinen
 * @returns {boolean}
 */
FontWriter.prototype.isDotDisplayMode = function () {
    return this.setDisplayMode === "dotmatix";
};

/**
 * Soll im LCD Modus erscheinen
 * @returns {boolean}
 */
FontWriter.prototype.isLCDDisplayMode = function () {
    return this.setDisplayMode === "LCD";
};

/**
 * Holt die Font von der Library und setzt Sie
 * @param fontName
 */
FontWriter.prototype.assignFontFamily = function (fontName) {
    var me = this,
        font = me.fontLibrary.getFont(fontName);
    if (font !== undefined) {
        me.setFont(font);
    }
};

/**
 * Holt per Ajax den Schrift Type vom fontConverter.php und setzt sie
 * @param fontName
 */
FontWriter.prototype.assignAjaxFontFamily = function (fontName) {
    var me = this,
        ajax = new AjaxCall(),
        url = "/fontConverter.php?font=" + encodeURI(fontName);

    ajax.get(url, function (XMLHttpRequest) {
        var fontJson = JSON.parse(XMLHttpRequest.responseText);
        if (fontJson) {
            me.setFont(fontJson);
        }
    });
};
