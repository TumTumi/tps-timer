/**
 * Created by tobi on 11.04.15.
 */

/**
 * Für einen AjaxCall
 * @constructor
 */
function AjaxCall() {
    this.xhr = new XMLHttpRequest();
}

/**
 * Startet den Ajax Call
 * @param url
 * @param succes
 */
AjaxCall.prototype.get = function (url, succes) {
    var me = this;
    me.xhr.onreadystatechange = function () { me.readystatechange(this, succes); };
    me.xhr.open('GET', url, true);
    me.xhr.send();
};

/**
 * Callback bei erfolgreichem Call
 * @param XMLHttpRequest
 * @param succes
 */
AjaxCall.prototype.readystatechange = function (XMLHttpRequest, succes) {
    if (XMLHttpRequest.readyState === 4 && XMLHttpRequest.status === 200) {
        succes(XMLHttpRequest);
    }
};