/**
 * Created by tobi on 10.04.15.
 */

function DisplayEngine(TimeEngine) {
    var me = this;
    me.htmlBox = document.getElementById('matrix');
    me.warnbox = me.createWarnbox();
    me.timeEngine = TimeEngine;
    me.fontWriter = new FontWriter();
}

/**
 * Aktualliesiert das Display
 * Berechnet automischt was gezeigt werden muss
 */
DisplayEngine.prototype.refresh = function () {
    var me = this;
    if (me.timeEngine.isCounterEnd()) {
        me.showWarning();
    } else {
        me.showTime();
    }
};

/**
 * Zeigt die Warnmeldugn an
 */
DisplayEngine.prototype.showWarning = function () {
    var me = this;
    me.warnbox.show(me.htmlBox);
};

/**
 * Zeigt die Uhrzeit an
 */
DisplayEngine.prototype.showTime = function () {
    var me = this;
    me.htmlBox.innerHTML = me.fontWriter.printNummer(me.timeEngine.calcTime());
};

/**
 * BackUp die HTML Element für die Warnbox
 * @returns {Warnbox}
 */
DisplayEngine.prototype.createWarnbox = function () {
    return new Warnbox();
};

/**
 * Leert den Bildschirm
 */
DisplayEngine.prototype.clear = function () {
    var me = this;
    me.warnbox.hide();
    me.htmlBox.innerHTML = "";
};

/**
 * Schaltet das Display aus
 */
DisplayEngine.prototype.sleepDisplay = function () {
    this.clear();
};
