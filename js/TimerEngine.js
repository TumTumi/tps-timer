/**
 * Created by tobi on 09.04.15.
 */

function TimerEngine() {
    this.timestamp = 0;
    this.timestarget = 0;
    this.nonStoptime = false;
    this.intervalID = null;
    this.timermode = "counter";
    this.display = null;
    this.counterEnd = false;
    this.domConfigTimemode = null;
    this.init();
}

TimerEngine.prototype.init = function () {
    var me = this,
        buttons = document.getElementsByTagName("button"),
        button_func = function (event) { me.event_buttonclick(event, this); },
        configTimerMode = me.getDomConfigTimeMode(),
        configTimerMode_func = function (event) { me.event_clockmodechange(me, event, this); },
        i;

    me.display = new DisplayEngine(me);

    for (i = 0; i < buttons.length; i++) {
        buttons[i].onclick = button_func;
    }

    configTimerMode.onchange = configTimerMode_func;
};

/**
 * Gibt das Select Element zuruck mit der einstellungen welchen mode.
 * @returns {HTMLDocument}
 */
TimerEngine.prototype.getDomConfigTimeMode = function () {
    var me = this;
    if (me.domConfigTimemode === null) {
        me.domConfigTimemode = document.getElementById('configClockMode');

    }
    return me.domConfigTimemode;
};

/**
 * Funktion für die Buttons
 *
 * @param event
 * @param button
 */
TimerEngine.prototype.event_buttonclick = function (event, button) {
    var me = this,
        action = button.innerHTML,
        configTimerMode = me.getDomConfigTimeMode();

    me.resetDisplay();

    switch (action) {
    case "start":
        me.setTimerMode();
        this.nonStoptime = true;
        me.startTimer(0);
        break;
    case "stop":
        this.nonStoptime = false;
        me.stopTimer();
        break;
    default:
        me.setClockModeByConfig(configTimerMode);
        me.startTimer(Number(action));
    }
};

/**
 * Funktion für den Pendel
 */
TimerEngine.prototype.event_timerintervall = function () {
    var me = this;
    if (me.nonStoptime === false && me.timestamp >= me.timestarget) {
        me.setCounterEnd(true);
        me.stopTimer();
    }
    me.timestamp += 1;
    me.updateDisplay();
};

/**
 * Funktion für Config Timer mode
 */
TimerEngine.prototype.event_clockmodechange = function (me, event, select) {
    if (me.nonStoptime === false) {
        me.setClockModeByConfig(select);
    };
};

/**
 * Akktualisiert das Display
 */
TimerEngine.prototype.updateDisplay = function () {
    var me = this;
    me.display.refresh();
};

/**
 * Stellt das Display aus
 */
TimerEngine.prototype.resetDisplay = function () {
    var me = this;
    me.display.sleepDisplay();
};

/**
 * Setzt den Modus als Counter und zählt ab
 * @returns {TimerEngine}
 */
TimerEngine.prototype.setCounterMode = function () {
    var me = this;
    me.timermode = "counter";

    return me;
};

/**
 * Setzt den Modus als Zähler und zählt auf.
 * @returns {TimerEngine}
 */
TimerEngine.prototype.setTimerMode = function () {
    var me = this;
    me.timermode = "timer";

    return me;
};

/**
 * Wenn der Counter abgelaufen ist wird gewarnt
 * @param boolean
 * @returns {TimerEngine}
 */
TimerEngine.prototype.setCounterEnd = function (boolean) {
    var me = this;
    me.counterEnd = boolean;

    return me;
};

/**
 * Prüft ob es im Counter Modus ist
 * @returns {boolean}
 */
TimerEngine.prototype.isCounterMode = function () {
    var me = this;
    return me.timermode === "counter";
};

/**
 * Prüft ob der Counter abgelaufen ist
 * @returns {boolean}
 */
TimerEngine.prototype.isCounterEnd = function () {
    var me = this;
    return me.counterEnd;
};

/**
 * Prüft ob es im Zähler Modus ist
 * @returns {boolean}
 */
TimerEngine.prototype.isTimerMode = function () {
    var me = this;
    return me.timermode === "timer";
};

/**
 * Startet den Timer
 * @param time
 */
TimerEngine.prototype.startTimer = function (time) {
    var me = this;
    me.stopTimer();
    me.setCounterEnd(false);
    me.setTimestamp(time);
    me.intervalID = window.setInterval(
        function () { me.event_timerintervall(); },
        1000
    );
    me.updateDisplay();
};

/**
 * Setzt die Eigenschaft fuer Zeit berechung
 */
TimerEngine.prototype.setTimestamp = function (time) {
    var me = this;
    //Setzt die Zeit Neu
    me.timestamp = 0; // Eins damit mit er bei eins anfängt nicht null;
    me.timestarget = time * 60;
};


/**
 * Stopt den Timer
 */
TimerEngine.prototype.stopTimer = function () {
    var me = this;
    if (me.intervalID !== null) {
        window.clearInterval(me.intervalID);
        me.intervalID = null;
    }
};

/**
 * Berechnet die Zahl die angezeigt wird
 * @returns {string}
 */
TimerEngine.prototype.calcTime = function () {
    var me = this,
        prefix = "",
        timestamp = Number(me.timestamp),
        min = 0,
        sec = 0;

    if (me.isCounterMode()) {
        prefix = "-";
        timestamp = Number(me.timestarget - me.timestamp);
    }

    min = Math.floor(timestamp / 60);
    sec = timestamp % 60;

    return prefix + min + ":" + me.twoDigit(sec);
};

/**
 * Gibt zwei stellige Zahlen aus
 * @param number
 * @returns {string}
 */
TimerEngine.prototype.twoDigit = function (number) {
    var twodigit = number >= 10 ? number.toString() : "0" + number.toString();
    return twodigit;
};

/**
 * Anhand der Config des select wird dir Clock eingestellt, geht auch im Live betrieb.
 * @param configTimerMode Ist der select-Tag aus dem DOM
 */
TimerEngine.prototype.setClockModeByConfig = function (configTimerMode) {
    var me = this,
        option = configTimerMode[configTimerMode.selectedIndex],
        config = option.value,
        funcname = "set" + config.charAt(0).toUpperCase() + config.slice(1) + "Mode";

    if (me[funcname]) {
        me[funcname]();
    } else {
        console.log("Unbekannter Config Mode: " + config);
    }

};

window.onload = function () {
    var timerEngine = new TimerEngine();
};
